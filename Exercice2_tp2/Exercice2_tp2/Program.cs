﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice2_tp2
{
    public class Program
    {
        public static bool PlusieurValeurEgale(int[] tableau_de_valeur, int ValeurChercher, int NombreValeurEgale)
        {
            int Compteur, CompteurValeurEgale=0;
            for (Compteur = 0; Compteur < 10; Compteur++)
            {
                if (tableau_de_valeur[Compteur] == ValeurChercher)
                {
                    CompteurValeurEgale++;
                    Console.WriteLine("{0} est la {1}-eme valeur saisie.", ValeurChercher, Compteur + 1);
                }
            }
            if (CompteurValeurEgale == NombreValeurEgale)
            {
                return true;
            }
            return false;
        }
        public static bool AuMoins1ValeurEgale(int[] tableau_de_valeur, int ValeurChercher)
        {
            int NombreValeurEgale = 0, PositionValeur=0;
            for (int Compteur = 0; Compteur < 10; Compteur++)
            {
                if (tableau_de_valeur[Compteur] == ValeurChercher)
                {
                    NombreValeurEgale++;
                    PositionValeur = Compteur;
                }
            }
            if (NombreValeurEgale == 1)
            {
                Console.WriteLine("{0} est la {1}-eme valeur saisie.", ValeurChercher, PositionValeur + 1);
                return true;
            }
            if (NombreValeurEgale > 1)
            {
                PlusieurValeurEgale(tableau_de_valeur, ValeurChercher, NombreValeurEgale);
            }
            return false;
        }
        static void Main(string[] args)
        {
            int[] tableau_de_valeur = new int[10];
            int valeur_chercher, valeur_tableau;
            int compteur;
            string valeur_entre, valeur_chercher_as_string;
            Console.WriteLine("Saissisez dix valeur :");
            for (compteur = 0; compteur < 10; compteur++)
            {
                Console.Write("{0} : ", compteur + 1);
                valeur_entre = Console.ReadLine();
                int.TryParse(valeur_entre, out valeur_tableau);

                tableau_de_valeur[compteur] = valeur_tableau;
            }
            Console.WriteLine("Saisissez une valeur :");

            valeur_chercher_as_string = Console.ReadLine();
            int.TryParse(valeur_chercher_as_string, out valeur_chercher);
            AuMoins1ValeurEgale(tableau_de_valeur, valeur_chercher);
            Console.ReadKey();
        }
    }
}
