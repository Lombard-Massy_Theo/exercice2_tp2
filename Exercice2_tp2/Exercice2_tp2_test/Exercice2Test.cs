﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercice2_tp2;
namespace Exercice2_tp2_test
{
    [TestClass]
    public class Exercice2Test
    {
        [TestMethod]
        public void TestAuMoins1ValeurEgale()
        {
            int[] tableau_de_valeur = new int[10] { 50, 20, 30, 40, 50, 60, 70, 80, 90, 50};
            Assert.IsTrue(Program.AuMoins1ValeurEgale(tableau_de_valeur,20));
        }
        [TestMethod]
        public void TestPlusieurValeurEgale()
        {
            int[] tableau_de_valeur = new int[10] { 50, 20, 30, 40, 50, 60, 70, 80, 90, 50 };
            Assert.IsTrue(Program.PlusieurValeurEgale(tableau_de_valeur, 50,3));
        }
    }
}
